All files in this directory were written by me with the excpetion of the following files:

PI.glsl, interpolate.glsl, pass-through.fs, passs-through.tcs, pass-through.vs, pass-through.glsl, 
random2.glsl, smooth_heaviside.glsl and version410.glsl

To run the build, use ./shaderpipeline followed by the json file. There are 8 tests json files in data, labeled like this: test-08.json. 

For the final product, use json test 8:

./shaderpipeline ../data/test-08.json

It may take a bit for the shaders to compile. The terminal might give a warning with the option to terminate the program or wait. If you click wait, the program should start shortly afterward.
