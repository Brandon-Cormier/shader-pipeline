//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//NOTICE!
//I declared a function inside of bump_height.... because that's where I was calling perlin
//and I wanted to directly adjust the noise using octaves with frequency and amplitude
//Also: You may need to wait a minute for the shader to load, runing the octaves takes a few seconds
//When I run it, I need to click "wait" from an alert on the terminal for it to run, it always runs right after clicking wait for me
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Set the pixel color using Blinn-Phong shading (e.g., with constant blue and
// gray material color) with a bumpy texture.
// 
// Uniforms:
uniform mat4 view;
uniform mat4 proj;
uniform float animation_seconds;
uniform bool is_moon;
// Inputs:
//                     linearly interpolated from tessellation evaluation shader
//                     output
in vec3 sphere_fs_in;
in vec3 normal_fs_in;
in vec4 pos_fs_in; 
in vec4 view_pos_fs_in; 
// Outputs:
//               rgb color of this pixel
out vec3 color;
// expects: model, blinn_phong, bump_height, bump_position,
// improved_perlin_noise, tangent
void main()
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //NOTICE!
  //I declared a function inside of bump_height.... because that's where I was calling perlin
  //and I wanted to directly adjust the noise using octaves with frequency and amplitude
  //Also: You may need to wait a minute for the shader to load, runing the octaves takes a few seconds
  //When I run it, I need to click "wait" from an alert on the terminal for it to run, it always runs right after clicking wait for me
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  float theta = (-2.0*M_PI/8.0)*animation_seconds;
  vec4 light = vec4(5, 5, 5, 1);
  mat4 rotation = mat4(cos(theta), 0, -sin(theta), 0,
            0, 1, 0, 0,
            sin(theta), 0, cos(theta), 0,
            0, 0, 0, 1);
  light = view *rotation*light;

  mat4 model = model(is_moon, animation_seconds);
  
  //tangents
  vec3 T, B;
  tangent(sphere_fs_in, T, B);


  //normal and bump map from assignment handout
  vec3 new_position = bump_position(is_moon, sphere_fs_in);

  vec3 normal_prime = normalize(cross((bump_position(is_moon, sphere_fs_in + 0.0001*T) - new_position)/0.0001,(bump_position(is_moon, sphere_fs_in + 0.0001*B) - new_position)/0.0001)); 

  vec4 new_view = view*model*vec4(new_position, 1);
  vec3 new_normal = (transpose(inverse(view))*transpose(inverse(model))*vec4(normal_prime, 1.0)).xyz;

 
  

  if (is_moon) {
      color = blinn_phong(vec3(0.05, 0.05, 0.05),
                          vec3(0.5, 0.5, 0.5),
                          vec3(1, 1, 1),
                          2500,
                          normalize(new_normal),
                          normalize(-new_view.xyz),
                          normalize(light.xyz-new_view.xyz));
  } else {
    color = blinn_phong(vec3(0, 0, 0.1),
                          vec3(0.2, 0.2, 1),
                          vec3(1, 1, 1),
                          1500,
                          normalize(new_normal),
                          normalize(-new_view.xyz),
                          normalize(light.xyz-new_view.xyz));
  }
  /////////////////////////////////////////////////////////////////////////////
}
