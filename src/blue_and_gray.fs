// Set the pixel color to blue or gray depending on is_moon.
//
// Uniforms:
uniform bool is_moon;
// Outputs:
out vec3 color;
void main()
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code:
  if(is_moon){
    //the moon is grey
    color = vec3(0.5,0.5,0.5);
    return;
  }
  //the planet is blue
  color = vec3(0,0,1);
  /////////////////////////////////////////////////////////////////////////////
}
