// Construct the model transformation matrix. The moon should orbit around the
// origin. The other object should stay still.
//
// Inputs:
//   is_moon  whether we're considering the moon
//   time  seconds on animation clock
// Returns affine model transformation as 4x4 matrix
//
// expects: identity, rotate_about_y, translate, PI
mat4 model(bool is_moon, float time)
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code 
  //spin to win! (planet isn't spinning). scale the moon to 0.3
  float theta = (2.0*M_PI/4.0)*time;
  vec3 translation = vec3(2*sin(theta), 0, 2*cos(theta));
  if(is_moon){
    return translate(translation)*rotate_about_y(theta) * uniform_scale(0.3);
  }
  else{
    return identity();
  }
  /////////////////////////////////////////////////////////////////////////////
}
