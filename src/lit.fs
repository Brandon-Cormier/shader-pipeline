// Add (hard code) an orbiting (point or directional) light to the scene. Light
// the scene using the Blinn-Phong Lighting Model.
//
// Uniforms:
uniform mat4 view;
uniform mat4 proj;
uniform float animation_seconds;
uniform bool is_moon;
// Inputs:
in vec3 sphere_fs_in;
in vec3 normal_fs_in;
in vec4 pos_fs_in; 
in vec4 view_pos_fs_in; 
// Outputs:
out vec3 color;
// expects: PI, blinn_phong
void main()
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code 

    //8 seconds per rotation, roughly what's in the handout
    float theta = (-2.0*M_PI/8.0)*animation_seconds;
    //arbitrary location for the light... sorta looks like where it is in the handout
    vec4 light = vec4(5, 5, 5, 1);
    //rotate about Y
    mat4 rotation = mat4(cos(theta), 0, -sin(theta), 0,
              0, 1, 0, 0,
              sin(theta), 0, cos(theta), 0,
              0, 0, 0, 1);
    light = view * rotation*light;
    
    if (is_moon) {
        color = blinn_phong(vec3(0.05, 0.05, 0.05),
                            vec3(0.5, 0.5, 0.5),
                            vec3(1, 1, 1),
                            2500,
                            normalize(normal_fs_in),
                            normalize(-view_pos_fs_in.xyz),
                            normalize(light.xyz-view_pos_fs_in.xyz));
    } else {
      color = blinn_phong(vec3(0, 0, 0.1),
                            vec3(0.2, 0.2, 1),
                            vec3(1, 1, 1),
                            1500,
                            normalize(normal_fs_in),
                            normalize(-view_pos_fs_in.xyz),
                            normalize(light.xyz-view_pos_fs_in.xyz));
    }
  /////////////////////////////////////////////////////////////////////////////
}
