// Generate a pseudorandom unit 3D vector
// 
// Inputs:
//   seed  3D seed
// Returns psuedorandom, unit 3D vector drawn from uniform distribution over
// the unit sphere (assuming random2 is uniform over [0,1]²).
//
// expects: random2.glsl, PI.glsl
vec3 random_direction( vec3 seed)
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code 
  vec2 random = random2(seed);

  //Mercator projection
  //https://en.wikipedia.org/wiki/Mercator_projection
  //https://stackoverflow.com/questions/12732590/how-map-2d-grid-points-x-y-onto-sphere-as-3d-points-x-y-z
  float longitude = -2*M_PI*random.x;
  float latitude = 2 * acos(2*random.y-1) - M_PI/2;
  float x = cos(latitude) * cos(longitude);
  float y = cos(latitude) * sin(longitude);
  float z = sin(latitude);
  return normalize(vec3(x,y,z));
  /////////////////////////////////////////////////////////////////////////////
}
